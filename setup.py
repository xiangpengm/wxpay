import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()


setuptools.setup(
    name="WXPay",
    version="0.1",
    author="wangxiangpeng",
    author_email="xiangpengm@126.com",
    description='A Wechat pay package',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/xiangpengm/WXPay",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    classifiers=(
        "Programming Language :: Python :: 3.6",
        "License :: OSIApproved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=[
        'requests',
        'flask',
        'xmltodict',
    ],

)
