//index.js
//获取应用实例
const app = getApp()

Page({
 data: {
     item:{
         index: 0,
         msg: 'this is template',
         time: 'Now'
     },
     cells:[
         {
             description: "小胖子",
             price: 5,
             members:3
         },
         {
             description: "大褂",
             price: 10,
             members: 5
         }
     ]
 },
 pay: function(){
     wx.login({
      success: function(res) {
        if (res.code) {
          //发起网络请求
          wx.request({
            url: 'https://api.xiangpengm.cn/login',
            data: {
              code: res.code
            },
            success: function(res){
                console.log(res.data);
                app.globalData['openid']=res.data['openid']
            }
        },
    )
          console.log('登录成功');
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
  })
},
 unifiedorder: function(){
     wx.request({
         url: 'https://api.xiangpengm.cn/unifiedorder',
         data: {
            openid: app.globalData.openid
        },
        success: function(res){
            console.log('请求成功');
            if (res.data.pay == 'ok'){
                wx.requestPayment({
                   'timeStamp': res.data.timeStamp,
                   'nonceStr': res.data.nonceStr,
                   'package': res.data.package,
                   'signType': 'MD5',
                   'paySign': res.data.paySign,
                   'success':function(res){
                        console.log(res);
                   },
                   'fail':function(res){
                        console.log(res);
                   }
               })
           } else {
               console.log('failed');
           }
        },
        fail: function(){
            console.log('fail');
        }
    })
},
 getIp: function(){
     wx.request({
  url: 'http://ip-api.com/json',
  success:function(e){
    console.log(e.data);
    that.setData({
      motto:e.data
    })
  }
})
},
showToast: function(){
wx.showToast({
  title: '成功',
  icon: 'loading',
  duration: 2000
})
},
showLoading: function(){
    wx.showLoading({
  title: '加载中',
  mask: true,
})

setTimeout(function(){
  wx.hideLoading()
},2000)
}
})
