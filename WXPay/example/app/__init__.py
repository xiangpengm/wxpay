from flask import Flask
from flask import request
from datetime import datetime
import xmltodict
import json
from WXPay import Wxpay
from WXPay.utils import parse_json
from argparse import ArgumentParser


"""
TODO: 封装请求接口
"""
# 设置命令行参数
parse = ArgumentParser()
parse.add_argument('-config', dest='config', required=True, help='please input config json file')
arg = parse.parse_args()
# 解析配置文件
pay_config = parse_json(arg.config)

# 实例化
app = Flask(__name__)
pay = Wxpay(**pay_config)


def save_file(file, data):
    with open(file, 'a') as f:
        print(data, file=f)


@app.route('/login')
def login():
    # 获取openid
    # 传入请求
    # 在这里实现自定义登录状态
    # session_key 和 openid
    # session_key 需要存储在服务器
    # openid 下发到 小程序
    # 每次获取数据小程序都要验证 openid 如果没有则说明没有登录
    response = pay.login(request.args.get('code'))
    return response


@app.route('/unifiedorder')
def unifiedorder():
    """
    下单
    :return:
    :rtype:
    """
    dt = dict(
        device_info="web",
        body = 'xiangpeng',
        spbill_create_ip= request.headers.get('X-Real-Ip'),
        out_trade_no= datetime.strftime(datetime.now(),"%Y%m%d%H%M%s"),
        total_fee = 1,
        notify_url = "https://api.xiangpengm.cn/notify_url",
        openid = request.args.get('openid')
    )
    return pay.unified_order(**dt)


@app.route('/orderquery')
def orderquery():
    dt = dict(
        transaction_id='transaction_id'
    )
    return pay.order_query(**dt)


@app.route('/notify_url', methods=['POST'])
def notify_url():
    xml = request.data.decode()
    dt = xmltodict.parse(xml)
    # {"xml": {"appid": "wx1a65371cf818a10c", "bank_type": "CFT", "cash_fee": "1", "device_info": "web", "fee_type": "CNY", "is_subscribe": "N", "mch_id": "1504212501", "nonce_str": "Y60T1CNR2NEKTMHGAFSLLQJ1F9WLNXRN", "openid": "oZWfW5f-_q_PjPH8TUQ5D7bGc1v4", "out_trade_no": "2018052906301527546627", "result_code": "SUCCESS", "return_code": "SUCCESS", "sign": "260301C8565BABF2DCB11CFB1F0D6158", "time_end": "20180529063046", "total_fee": "1", "trade_type": "JSAPI", "transaction_id": "4200000119201805290399186883"}}

    save_file('status.txt', json.dumps(dt, ensure_ascii=False))
    result = """<xml>
                    <return_code><![CDATA[SUCCESS]]></return_code>
                    <return_msg><![CDATA[OK]]></return_msg>
                </xml>
            """
    return result

@app.route('/pay.action', methods=['POST'])
def notify_url():
    xml = request.data.decode()
    dt = xmltodict.parse(xml)
    # {"xml": {"appid": "wx1a65371cf818a10c", "bank_type": "CFT", "cash_fee": "1", "device_info": "web", "fee_type": "CNY", "is_subscribe": "N", "mch_id": "1504212501", "nonce_str": "Y60T1CNR2NEKTMHGAFSLLQJ1F9WLNXRN", "openid": "oZWfW5f-_q_PjPH8TUQ5D7bGc1v4", "out_trade_no": "2018052906301527546627", "result_code": "SUCCESS", "return_code": "SUCCESS", "sign": "260301C8565BABF2DCB11CFB1F0D6158", "time_end": "20180529063046", "total_fee": "1", "trade_type": "JSAPI", "transaction_id": "4200000119201805290399186883"}}

    save_file('status.txt', json.dumps(dt, ensure_ascii=False))
    result = """<xml>
                    <return_code><![CDATA[SUCCESS]]></return_code>
                    <return_msg><![CDATA[OK]]></return_msg>
                </xml>
            """
    return result


# https://pay.weixin.qq.com/wxpay/pay.action