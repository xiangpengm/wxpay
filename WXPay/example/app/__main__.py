from WXPay.example import app


if __name__ == '__main__':
    config = dict(
        debug=True,
        host='0.0.0.0',
        port=2001,
    )
    app.run(**config)
