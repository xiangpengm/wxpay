from hashlib import md5
import hmac
import json

__all__ = ['get_hmac_sha256', 'get_md5', 'raw_dt', 'gene_make_sign']


def parse_json(file):
    with open(file, 'r') as f:
        r =  json.loads(f.read())
        return r


def get_md5(s):
    """
    获取字符串 md5 签名
    :param s: 要签名的字符串
    :type s: str
    :return: 签名后的字符串 32位
    :rtype: str
    """
    m = md5()
    m.update(s.encode())
    return m.hexdigest().upper()


def get_hmac_sha256(s, key):
    """
    获取字符串 HMAC-SHA256签名
    :param s: 要签名的字符串
    :type s: str
    :param key: 商户使用的密钥
    :type key: str
    :return: 签名后的字符串 64位
    :rtype: str
    """
    m = hmac.new(key.encode(), s.encode(), 'sha256')
    return m.hexdigest().upper()


def raw_dt(x):
    """
    把字典的key变成xml里面的Raw 字符，防止被转义
    :param x: data
    :type x: str
    :return: xml raw
    :rtype: str
    """
    return  '<![CDATA[{}]]>'.format(x)


def make_md5_sign(dt, secret_key):
    """
    使用md5签名
    :param dt: md5 签名的字典
    :type dt: dict
    :param secret_key: 密钥
    :type secret_key: str
    :return: 返回签名的字符串
    :rtype: str
    """
    StringpaySign = '&'.join(('{}={}'.format(k, v) for k, v in sorted(dt.items())))
    stringSignTemp = StringpaySign + "&key={}".format(secret_key)
    print(stringSignTemp)
    return get_md5(stringSignTemp)


def make_sha256_sign(dt, secret_key):
    """
    使用hmac-sha256 签名
    :param dt: 签名的字典
    :type dt: dict
    :param secret_key: 密钥
    :type secret_key: str
    :return: 返回签名的字符串
    :rtype: str
    """
    StringpaySign = '&'.join(('{}={}'.format(k, v) for k, v in sorted(dt.items())))
    stringSignTemp = StringpaySign + "&key={}".format(secret_key)
    return get_hmac_sha256(stringSignTemp, secret_key)


gene_make_sign = {
    'MD5': make_md5_sign,
    'HMAC-SHA256': make_sha256_sign
}




