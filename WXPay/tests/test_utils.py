from WXPay.utils import *
from WXPay import Wxpay
from argparse import ArgumentParser
from WXPay.utils import parse_json
parse = ArgumentParser()
parse.add_argument('-config', dest='config', required=True, help='please input config json file')
arg = parse.parse_args()
pay_config = parse_json(arg.config)
wxpay = Wxpay(**pay_config)


def test_sign():
    """
    测试签名与微信官网示例是否一致
    :return:
    :rtype:
    """
    dt = {
        'appid': 'wxd930ea5d5a258f4f',
        'mch_id': '10000100',
        'device_info': '1000',
        'body': 'test',
        'nonce_str': 'ibuaiVcKdpRxkhJA',
    }
    secret_key = '192006250b4c09247ec02edce69f6a2d'
    md5_sign = gene_make_sign('MD5')
    hmac_sha256 = gene_make_sign('HMAC-SHA256')
    print(md5_sign(dt, secret_key))
    print(hmac_sha256(dt, secret_key))
    assert md5_sign(dt, secret_key)=='9A0A8659F005D6984697E2CA0A9CF3B7','9A0A8659F005D6984697E2CA0A9CF3B7'
    assert hmac_sha256(dt,secret_key)=='6A9AE1657590FD6257D693A078E1C3E4BB6BA4DC30B23E0EE2496E54170DACD6', '6A9AE1657590FD6257D693A078E1C3E4BB6BA4DC30B23E0EE2496E54170DACD6'


def test_order_query():
    # 解析配置文件
    # 实例化
    print(wxpay.order_query('4200000119201805290399186883'))

def test_sec_api_pay_refund():
    print(wxpay.sec_api_pay_refund(
        transaction_id="4200000129201805214547593648",
        out_refund_no="2018052116511526892716",
        total_fee=1,
        refund_fee=1,
    ))

def test_refund_query():
    print(wxpay.refund_query(transaction_id='4200000129201805214547593648'))

def test_downloadBill():
    print(wxpay.download_bill('20180519','ALL'))

def test_download_fundFlow():
    print(wxpay.download_fundflow('20180519', 'Basic'))

def test_batch_query_comment():
    print(wxpay.batch_query_comment('20180520000000', '20180708000000'))


if __name__ == '__main__':
    # test_sign()                # 测试通过
    # test_order_query()         # 测试通过
    # test_sec_api_pay_refund()  # 测试通过
    # test_refund_query()        # 测试通过
    # test_downloadBill()        # 测试通过
    # test_download_fundFlow()   # 测试通过
    # test_batch_query_comment() # 测试通过
    pass