from requests import Session
import string
import random
import xmltodict
import json
import time
from WXPay.utils import *
import urllib3


__all__ = ['Wxpay']
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
TODO: 1. 微信支付api对接
      2. 文化部导游信息查询接口: 客户端当作代理请求文化部官网, 把数据再发送回后台服务器
      3. 短信接口对接
      4. 驾驶证接口对接 
      5. 实名认证, 身份证或银行卡
"""

class Wxpay(Session):
    def __init__(self, appid, mch_id, secret, cert, key,
                 grant_type='authorization_code', unified_order_notify_url=None,
                 sec_api_pay_refund_notify_url=None):
        super().__init__()
        self.appid = appid
        self.mch_id = mch_id
        self.SECRET = secret
        self.grant_type = grant_type
        # self.sign_type = sign_type
        self.cert = cert
        self.key = key
        self.make_sign=gene_make_sign
        # 统一下单提交的url
        self.unified_order_notify_url =  unified_order_notify_url
        # 退款结果提交的url
        self.sec_api_pay_refund_notify_url = sec_api_pay_refund_notify_url

    @property
    def nonce_str(self):
        """
        生成随机的32位字符串
        :param k:
        :type k: int
        :return:
        :rtype: str
        """
        seed = string.ascii_uppercase + string.digits
        return ''.join(random.choices(seed, k=32))

    def __sign(self, dt, url, verify=True, sign_type='MD5'):
        """
        数据签名-> 转成xml -> 发送请求 -> 返回响应
        :param dt: 需要签名的数据
        :type dt: dict
        :param url: 发送请求的url
        :type url: str
        :param verify: 是否使用双向证书验证
        :type verify: bool
        :return: 返回响应的原始数据
        :type return: str
        """
        dt = {k: v for k,v in dt.items() if v is not None}
        dt['sign'] = self.make_sign[sign_type](dt, self.SECRET)
        xml_dt = {"xml": dt}
        xml = xmltodict.unparse(xml_dt)
        result = self.post(url, data=xml.encode(),
                           cert=(self.cert, self.key),
                           verify=verify,
                           headers={'Content-Type': 'text/xml'})
        return result

    def login(self, code):
        """
        使用微信的临时登录凭证换取openid
        :param code: 临时登录code
        :return:
        """
        url = "https://api.weixin.qq.com/sns/jscode2session"
        dt = dict(
            js_code=code,
            appid=self.appid,
            SECRET=self.SECRET,
            grant_type=self.grant_type
        )
        code_api = url + "?appid={appid}&secret={SECRET}&js_code={js_code}&grant_type={grant_type}".format(
            **dt)
        return self.get(code_api).text

    def unified_order(self, body, out_trade_no, total_fee, spbill_create_ip, openid,
                     trade_type='JSAPI', sign_type="MD5", device_info=None, detail=None, attach=None,fee_type=None,
                     time_start=None,time_expire=None,goods_tag=None,product_id=None,limit_pay=None):
        """
        统一下单入口, 必须的参数未指定，非必须参数设置为None
        :param body: 商品描述
        :param out_trade_no: 商户订单号
        :param total_fee: 标价金额
        :param spbill_create_ip: 终端IP
        :param openid: 用户标识
        :param trade_type: 交易类型
        :param device_info: 设备号
        :param detail: 商品详情
        :param attach: 附加数据
        :param fee_type: 标价币种
        :param time_start: 交易起始时间
        :param time_expire: 交易结束时间
        :param goods_tag: 订单优惠标记
        :param product_id: 商品ID
        :param limit_pay: 指定支付方式
        :return: json结果
        """
        url = 'https://api.mch.weixin.qq.com/pay/unifiedorder'
        nonce_str = self.nonce_str
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            device_info=device_info,
            nonce_str=nonce_str,
            sign_type=sign_type,
            body=body,
            detail=detail,
            attach=attach,
            out_trade_no=out_trade_no,
            fee_type=fee_type,
            total_fee=total_fee,
            spbill_create_ip=spbill_create_ip,
            time_start=time_start,
            time_expire=time_expire,
            goods_tag=goods_tag,
            notify_url=self.unified_order_notify_url,
            trade_type=trade_type,
            product_id=product_id,
            limit_pay=limit_pay,
            openid=openid
        )
        result = self.__sign(dt, url)
        return_dict = xmltodict.parse(result.content.decode())
        # 得到签名的结果
        if return_dict['xml']['return_code'] == 'SUCCESS' and return_dict['xml']['return_code'] == 'SUCCESS':
            # 签名成功,返回数据给客户端, 客户端可以拉起支付
            result = dict(
                appId=self.appid,
                timeStamp=str(int(time.time())),
                nonceStr=nonce_str,
                package="prepay_id={}".format(return_dict['xml']['prepay_id']),
                signType=sign_type
            )
            result['paySign'] = self.make_sign[sign_type](result, self.SECRET)
            result['pay'] = 'ok'
            return json.dumps(result)
        else:
            # 签名失败返回的数据
            # 客户端收到返回的数据
            return json.dumps({'pay': 'fail'})

    def order_query(self,transaction_id, sign_type="MD5"):
        """
        订单查询入口,输入微信订单号,查询交易详细信息
        :param transaction_id:微信订单号
        :type transaction_id: str
        :return:
        """
        url = 'https://api.mch.weixin.qq.com/pay/orderquery'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            transaction_id=transaction_id,
        )
        result = self.__sign(dt, url)
        return xmltodict.parse(result.content.decode())

    def close_order(self, out_trade_no, sign_type):
        """
        关闭订单
        :param out_trade_no:
        :type out_trade_no: str
        :return:
        :rtype: str
        """
        url = 'https://api.mch.weixin.qq.com/pay/closeorder'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            out_trade_no=out_trade_no,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
        )
        result = self.__sign(dt, url)
        return xmltodict.parse(result.content.decode())

    def sec_api_pay_refund(self, transaction_id, out_refund_no, total_fee, refund_fee,
                          refund_fee_type='CNY',sign_type="MD5", refund_desc=None,
                          refund_account=None):
        """
        申请退款
        :return:
        """
        url = 'https://api.mch.weixin.qq.com/secapi/pay/refund'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            transaction_id=transaction_id,
            out_refund_no=out_refund_no,
            total_fee=total_fee,
            refund_fee=refund_fee,
            refund_fee_type=refund_fee_type,
            refund_desc=refund_desc,
            refund_account=refund_account,
            notify_url=self.sec_api_pay_refund_notify_url,
        )
        result = self.__sign(dt, url, verify=True)
        return xmltodict.parse(result.content.decode())

    def refund_query(self,transaction_id, offset=None, sign_type="MD5"):
        """
        查询退款
        :return:
        """
        url = 'https://api.mch.weixin.qq.com/pay/refundquery'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            transaction_id=transaction_id,
            offset=offset,
        )
        result = self.__sign(dt, url)
        return xmltodict.parse(result.content.decode())


    def download_bill(self, bill_date, bill_type, tar_type=None, sign_type="MD5"):
        """
        下载对账单
        :return:
        """
        url = 'https://api.mch.weixin.qq.com/pay/downloadbill'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            bill_date=bill_date,
            bill_type=bill_type,
            tar_type=tar_type,

        )
        result = self.__sign(dt, url)
        return result.content.decode()


    def download_fundflow(self, bill_date, account_type, tar_type=None, sign_type="HMAC-SHA256"):
        """
        下载资金账单
        :return:
        """
        url = 'https://api.mch.weixin.qq.com/pay/downloadfundflow'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            bill_date=bill_date,
            account_type=account_type,
            tar_type=tar_type,
        )
        result = self.__sign(dt, url, sign_type='HMAC-SHA256')
        return result.content.decode()


    def payitil_report(self, interface_url,execute_time, return_code,
                            return_msg, result_code,user_ip,
                            device_info=None, err_code=None, err_code_des=None,
                            out_trade_no=None,time=None, sign_type="MD5"):
        """
        提交交易的信息给微信的服务器,反馈问题
        交易保障
        :return:
        """
        url = 'https://api.mch.weixin.qq.com/payitil/report'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            device_info=device_info,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            interface_url=interface_url,
            execute_time=execute_time,
            return_code=return_code,
            return_msg=return_msg,
            result_code=result_code,
            err_code=err_code,
            err_code_des=err_code_des,
            out_trade_no=out_trade_no,
            user_ip=user_ip,
            time=time
        )
        result = self.__sign(dt, url)
        return xmltodict.parse(result.content.decode())


    def batch_query_comment(self,begin_time,end_time, offset=0, limit=200, sign_type="HMAC-SHA256"):
        url = 'https://api.mch.weixin.qq.com/billcommentsp/batchquerycomment'
        dt = dict(
            appid=self.appid,
            mch_id=self.mch_id,
            nonce_str=self.nonce_str,
            sign_type=sign_type,
            begin_time=begin_time,
            end_time=end_time,
            offset=offset,
            limit=limit
        )
        result = self.__sign(dt, url, sign_type=sign_type)
        return result.content.decode()


